const Task = require('../models/Task.js')

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if(error) {
            console.log(error)
            return false
        } else{
        return result
        }
    })
}


module.exports.createTask = (reqBody) => {
    let newTask = new Task({
        name: reqBody.name
    })

    return newTask.save().then((savedTask, error) => {
        if(error) {
            console.log(error)
            return false
        } else if (savedTask != null && savedTask.name == reqBody.name){
            return 'Duplicate Task Found'
        } else {
            return savedTask 
        }
    })
};



module.exports.updateStatus = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			result.status = newContent.status

			return result.save().then((updatedTask, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return updatedTask
				}
			})
		}
	})
}

